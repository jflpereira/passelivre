FROM quay.io/aptible/ruby:1.9.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs libsqlite3-dev
RUN mkdir /passe-livre
RUN gem install bundler
WORKDIR /passe-livre

ADD Gemfile /passe-livre/Gemfile
ADD Gemfile.lock /passe-livre/Gemfile.lock
RUN bundle install
ADD . /passe-livre