# encoding: UTF-8
module SessionsHelper

	def loga(usuario)
		session[:remember_token] = usuario.id
		self.usuario_atual = usuario
	end

	def logado?
		!usuario_atual.nil?
	end

	def desloga
		self.usuario_atual = nil
		session.delete(:remember_token)
	end

	def usuario_logado
		redirect_to root_path, notice: "É necessário estar logado." unless logado?
	end

	def usuario_atual=(usuario)
		@usuario_atual = usuario
	end

	def usuario_atual
		if session[:remember_token]
			@usuario_atual ||= Usuario.find(session[:remember_token])
		end
	end
end
