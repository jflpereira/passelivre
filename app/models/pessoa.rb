class Pessoa < ActiveRecord::Base
	validates :nome, presence: true
	validates :rg, presence: true, uniqueness: true
	validates :cpf, presence: true, uniqueness: true
	validates :data_nasc, presence: true
	validates :endereco, presence: true
	validates :numero, presence: true
	validates :bairro, presence: true
	validates :cidade, presence: true
	validates :estado, presence: true
	validates :cid, presence: true
	validates :descr_deficiencia, presence: true
	validates :emissor_laudo, presence: true
	validates :registro_emissor, presence: true
	validates :data_laudo, presence: true

	def getDataExpiracao
		if(self.data_expiracao.to_s.empty?)
			return (self.updated_at + 2.years).to_s(:data_abreviada)
		end

		self.data_expiracao.to_s(:data_abreviada)
	end
end
