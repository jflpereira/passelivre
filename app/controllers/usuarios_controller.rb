# encoding: UTF-8

class UsuariosController < ApplicationController
	before_action :usuario_admin

	def index
		@usuarios = Usuario.all
	end

	def show
		@usuario = Usuario.find(params[:id])
	end

	def new
		@usuario = Usuario.new
	end

	def create
		@usuario = Usuario.new(usuario_params)
		if @usuario.save
			flash[:success] = "Usuário criado com sucesso."
			redirect_to @usuario
		else
			render 'new'
		end

	end

	def edit
		@usuario = Usuario.find(params[:id])
	end

	def update
		@usuario = Usuario.find(params[:id])
		if @usuario.update_attributes(usuario_params)
			flash[:success] = "Alterações salvas."
			redirect_to @usuario
		else
			render 'edit'
		end

	end

	def destroy
		Usuario.find(params[:id]).destroy
		flash[:success] = "Usuário excluído."
		redirect_to usuarios_url
	end

	private
		def usuario_params
			params.require(:usuario).permit(:nome, :nome_completo, :password, :password_confirmation)
		end

		def usuario_admin
			redirect_to root_path,
				notice: "Somente administradores podem gerenciar usuários" unless usuario_atual.admin?
		end
end
