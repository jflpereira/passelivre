# encoding: UTF-8

class PessoasController < ApplicationController

	def index
		@pessoas = Pessoa.order("nome").paginate(page: params[:page])
	end

	def show
		@pessoa = Pessoa.find(params[:id])
		respond_to do |formato|
			formato.html
			formato.pdf do
				pdf = PessoaPdf.new(@pessoa, view_context)
				send_data pdf.render, filename:
					"credencial_#{@pessoa.rg}_#{@pessoa.created_at.strftime("%d-%m-%Y")}.pdf",
					type: "application/pdf"
			end
		end
	end

	def new
		@pessoa = Pessoa.new
	end

	def create
		@pessoa = Pessoa.new(pessoa_params)
		if @pessoa.save
			redirect_to @pessoa
		else
			render 'new'
		end
	end

	def edit
		@pessoa = Pessoa.find(params[:id])
	end

	def update
		@pessoa = Pessoa.find(params[:id])
		if @pessoa.update_attributes(pessoa_params)
			redirect_to @pessoa
		else
			render 'edit'
		end
	end

	def destroy
		Pessoa.find(params[:id]).destroy
		redirect_to pessoas_url
	end

	private
		def pessoa_params
			params.require(:pessoa).permit(:nome, :rg, :cpf, :data_nasc,
				:pai, :mae, :endereco, :numero, :complemento, :bairro,
				:cidade, :estado, :telefone, :cid, :descr_deficiencia,
				:emissor_laudo, :registro_emissor, :data_laudo,
				:acompanhante, :acomp_nome, :acomp_rg, :acomp_cpf, :data_expiracao)
		end
end
