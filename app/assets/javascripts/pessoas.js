function acompanhantePessoaCheckboxClick(event, target){
    var acompanhamentesCheckbox = $(target);
    var acompanhamentesContainer = $('.form-pessoa__acompanhates-info');

    if(acompanhamentesCheckbox.prop("checked") ){
        acompanhamentesContainer.fadeIn();
    }else{
        acompanhamentesContainer.fadeOut();
    }
}
