class AddIndexToUsuariosNome < ActiveRecord::Migration
  def change
  	add_index :usuarios, :nome, unique: true
  end
end
