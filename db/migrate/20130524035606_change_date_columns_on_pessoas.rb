class ChangeDateColumnsOnPessoas < ActiveRecord::Migration
  def change
  	change_column :pessoas, :data_nasc, :date
  	change_column :pessoas, :data_laudo, :date
  end
end
