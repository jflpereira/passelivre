class AdicionaIndicesAPessoas < ActiveRecord::Migration
  def change
  	add_index :pessoas, :rg, unique: true
  end
end
