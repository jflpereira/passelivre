class CreatePessoas < ActiveRecord::Migration
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :rg
      t.string :cpf
      t.datetime :data_nasc
      t.string :pai
      t.string :mae
      t.string :endereco
      t.string :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :telefone
      t.string :cid
      t.string :descr_deficiencia
      t.string :emissor_laudo
      t.string :registro_emissor
      t.datetime :data_laudo
      t.datetime :data_passe
      t.string :acompanhante
      t.string :acomp_rg
      t.string :acomp_cpf

      t.timestamps
    end
  end
end
