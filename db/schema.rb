# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180923042606) do

  create_table "pessoas", force: true do |t|
    t.string   "nome"
    t.string   "rg"
    t.string   "cpf"
    t.date     "data_nasc"
    t.string   "pai"
    t.string   "mae"
    t.string   "endereco"
    t.string   "numero"
    t.string   "complemento"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "estado"
    t.string   "telefone"
    t.string   "cid"
    t.string   "descr_deficiencia"
    t.string   "emissor_laudo"
    t.string   "registro_emissor"
    t.date     "data_laudo"
    t.datetime "data_passe"
    t.boolean  "acompanhante",      limit: 255
    t.string   "acomp_rg"
    t.string   "acomp_cpf"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "data_expiracao"
    t.string   "acomp_nome"
  end

  add_index "pessoas", ["rg"], name: "index_pessoas_on_rg", unique: true

  create_table "usuarios", force: true do |t|
    t.string   "nome"
    t.string   "nome_completo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.boolean  "admin",           default: false
  end

  add_index "usuarios", ["nome"], name: "index_usuarios_on_nome", unique: true

end
